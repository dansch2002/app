from app import app

from flask import render_template, request, redirect, jsonify, make_response

from datetime import datetime

@app.template_filter('clean_date')
def clean_date(dt):
    return dt.strftime("%d %b %Y")

@app.route("/")
def index():
    return render_template("public/index.html")

@app.route("/jinja")
def jinja():

    my_name = "Dan"

    age = 70

    langs = ["Python", "JavaScript", "Bash", "C", "Ruby"]

    friends = {
        "Ken": 71,
        "Vicky": 64,
        "Tony": 56,
        "Clarissa": 23
    }

    colors = ("Red", "Green")

    cool = True

    my_html = "<h1>THIS IS SOME HTML</h1>"

    class GitRemote:
        def __init__(self, name, description, url):
            self.name = name
            self.description = description
            self.url = url

        def pull(self):
            return f"Pulling repo {self.name}"

        def clone(self):
            return f"Cloning into {self.url}"

    my_remote = GitRemote(name="Flask Jinja",description="Template design tutorial",url="http://github.com/julian-nash/jinja.git")
    def repeat(x, qty):
        return x * qty

    date = datetime.utcnow()

    my_html = "<h1>THIS IS SOME HTML</h1>"

    suspicious = "<script>alert('YOU GOT HACKED')</script>"










    return render_template("public/jinja.html", my_name=my_name, age=age,
                           langs=langs, friends=friends, colors=colors,
                           cool=cool, GitRemote=GitRemote, repeat=repeat,
                           my_remote=my_remote, date=date, my_html=my_html,
                           suspicious=suspicious
                           )

@app.route("/about")
def about():
    return render_template("public/about.html")

@app.route("/signup", methods=["GET", "POST"])
def signup():

    if request.method == "POST":
        req = request.form

        username = req["username"]
        email = req.get("email")
        password = request.form["password"]

        print(username, email, password)

        return redirect(request.url)


    return render_template("public/sign_up.html")




users = {
    "billbobek": {
        "name": "Bill Bobek",
        "bio": "Much Music",
        "twitter_handle": "@bilbo"
    },
    "hclarke": {
        "name": "Ken Clarke",
        "bio": "Lead Guitar",
        "twitter_handle": "@lafcadio"
    },
    "dannyb": {
        "name": "Dan Blumberg",
        "bio": "Battleship Iowa",
        "twitter_handle": "@danb53"
    }

}

@app.route("/profile/<username>")
def profile(username):

    user = None
    if username in users:
        user = users[username]



    return render_template("public/profile.html",username=username, user=user)

@app.route("/multiple/<foo>/<bar>/<baz>")
def multi(foo,bar,baz):
    return f"foo is {foo}, bar is {bar}, baz is {baz}"

@app.route("/json", methods=["POST"])
def json():

    if request.is_json:

        req = request.get_json()

        response = {
            "message": "JSON received!",
            "name": req.get("name")
        }

        res = make_response(jsonify(response)), 200

        return res

    else:

        res = make_response(jsonify({"message": "No JSON received"}), 400)
        return res

@app.route("/guestbook")
def guestbook():
    return render_template("public/guestbook.html")

@app.route("/guestbook/create-entry", methods=["POST"])
def create_entry():

    req = request.get_json()
    print(req)
    #res = make_response(jsonify({"message": "JSON received"}),200)
    res = make_response(jsonify(req), 200)
    return res

@app.route("/query")
def query():

    # args = request.args

    # print(args)

    # for k, v in args.items():
    #    print(f"{k}:{v}")

    # if "foo" in args:
    #    foo = args.get("foo")

    # print(foo)

    if request.args:

        args = request.args

        # if "title" in args:
            # title = args.get()
            # title = args["title"]
            # title = request.args.get("title")
        #print(title)

        serialized = ", ".join(f"{k}: {v}" for k, v in args.items())



    # return "Query received", 200
        return f"(Query) {serialized}", 200

    else:

        return "No query received", 200

# foo=foo&bar=bar&baz=baz&query+strings+with+flask