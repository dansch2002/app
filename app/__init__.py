from flask import Flask

app = Flask(__name__)

from app import views #to avoid circular import
from app import admin_views